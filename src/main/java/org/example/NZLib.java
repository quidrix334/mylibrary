package org.example;

import java.util.*;

public class NZLib {

    private Map<String,String> commands;
    private Map<String, List<String>> reqArgs;
    private Map<String, List<String>> optArgs;

    public NZLib() {
        this.commands = new HashMap<>();
        this.reqArgs = new HashMap<>();
        this.optArgs = new HashMap<>();
    }

    public String registerCommand(String[] rawCommand){
        String command;
        List<String> optArgs = new ArrayList<>();
        List<String> reqArgs= new ArrayList<>();

        if(cmdMatcher(rawCommand[0])){
            command = rawCommand[0];
        }
        else {
            return "Invalid command";
        }

        for(int i=1;i<rawCommand.length;i++){
            if (optMatcher(rawCommand[i])){
                optArgs.add(rawCommand[i]);
                continue;
            }
            if (reqMatcher(rawCommand[i])){
                reqArgs.add(rawCommand[i]);
                continue;
            }
            return "Invalid command";
        }
        registrator(command,optArgs,reqArgs);
        return command + " is now registered";
    }

    public boolean commandIsValid(String rawCommand){
        String[] command = rawCommand.split(" ");
        int tempReq =0;
        if(commands.containsKey(command[0])){
            for(int i=1;i<command.length;i++){
                if(commands.containsKey(command[i])){
                    return false;
                }
                if(reqMatcher(command[i])){
                    tempReq++;
                }
            }
        }
        if(tempReq == reqArgs.get(command[0]).size()){
            return true;
        }
        return false;
    }

    public boolean reqMatcher(String s){
        if(s.matches("--[a-zA-Z]+")){
            return true;
        }
        return false;
    }
    public boolean optMatcher(String s){
        if(s.matches("-[a-zA-Z]+")){
            return true;
        }
        return false;
    }
    public boolean cmdMatcher(String s){
        if(s.matches("[a-zA-Z]+")){
            return true;
        }
        return false;
    }

    public String deleteCommand(String command){
        if(commands.containsKey(command)){
            commands.remove(command);
            if (reqArgs.containsKey(command)){
                reqArgs.remove(command);
            }
            if (optArgs.containsKey(command)){
                optArgs.remove(command);
            }
            return command + " has been deleted";
        }
        else{
            return "Command not found";
        }
    }


    private void registrator(String command,List<String> optArgs,List<String> reqArgs){
        this.commands.put(command,command);
        if(!optArgs.isEmpty()){
            this.optArgs.put(command,optArgs);
        }
        if(!reqArgs.isEmpty()){
            this.reqArgs.put(command,reqArgs);
        }

    }

    public List<String> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public List<String> getReqArgs(String command) {
        if(reqArgs.containsKey(command)){
            return new ArrayList<>(reqArgs.get(command));
        }
        return Collections.singletonList(command + " not found");
    }

    public List<String> getOptArgs(String command) {
        if(optArgs.containsKey(command)){
            return new ArrayList<>(optArgs.get(command));
        }
        return Collections.singletonList(command + " not found");
    }
}
