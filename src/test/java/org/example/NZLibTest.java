package org.example;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class NZLibTest {

    private Map<String,String> tCommands;
    private Map<String, List<String>> tReqArgs;
    private Map<String,List<String>> tOptArgs;
    private String valid=" is now registered";
    private String invalid ="Invalid command";

    NZLib lib;

    @Before
    public void init(){
        lib = new NZLib();
        tCommands = new HashMap<>();
        tReqArgs = new HashMap<>();
        tOptArgs = new HashMap<>();
        tCommands.put("myCommand","myCommand");
        tCommands.put("test","test");
        tReqArgs.put("myCommand", Arrays.asList("--h","--g"));
        tOptArgs.put("myCommand",Arrays.asList("-k"));
        tOptArgs.put("test",Arrays.asList("-x"));
        lib.registerCommand("myCommand --h --g -k".split(" "));
        lib.registerCommand("test -x".split(" "));
    }

    @Test
    public void registerCommand(){
        assertEquals("thisIsIt"+valid,lib.registerCommand("thisIsIt".split(" ")));
    }

    @Test
    public void registerInvalidSyntaxCommand(){
        assertNotEquals("thisIsIt"+valid,lib.registerCommand("-h thisIsIt".split(" ")));
    }

    @Test
    public void gettingAllRequiredArguments(){
        assertEquals(Arrays.asList("--h","--g"),lib.getReqArgs("myCommand"));
    }

    @Test
    public void regexMatcherForRequired(){
        assertTrue(lib.reqMatcher("--z"));
    }

    @Test
    public void falseRegexMatcherForOpt(){
        assertFalse(lib.reqMatcher("---z"));
    }
    @Test
    public void regexMatcherForCmd(){
        assertTrue(lib.cmdMatcher("quack"));
    }

    @Test
    public void falseRegexMatcherForCmd(){
        assertFalse(lib.cmdMatcher("---quack"));
    }
    @Test
    public void regexMatcherForOpt(){
        assertTrue(lib.optMatcher("-z"));
    }

    @Test
    public void falseRegexMatcherForRequired(){
        assertFalse(lib.reqMatcher("---z"));
    }

    @Test
    public void getAllCmds(){
        assertEquals(Arrays.asList("myCommand","test"),lib.getCommands());
    }

    @Test
    public void getAlloptArgs(){
        assertEquals(Arrays.asList("-x"),lib.getOptArgs("test"));
    }

    @Test
    public  void commandIsValid(){
        assertTrue(lib.commandIsValid("myCommand --h --g"));
    }

    @Test
    public void commandMissingOneReq(){
        assertFalse(lib.commandIsValid("myCommand --h"));
    }

    @Test
    public void commandMissingRequired(){
        assertFalse(lib.commandIsValid("myCommand"));
    }

    @Test
    public void testIfThereAreCommands(){
        assertEquals(2,lib.getCommands().size());
    }

    @Test
    public void checkIfCommandsGetDeleted(){
        lib.deleteCommand("myCommand");
        assertEquals(1,lib.getCommands().size());
        assertEquals(Collections.singletonList("myCommand not found"),lib.getOptArgs("myCommand"));
    }

    @Test
    public void checkIfOtherCommandsStayedTheSameAfterDeletion(){
        lib.deleteCommand("myCommand");
        assertEquals(Collections.singletonList("test"),lib.getCommands());
        assertEquals(1,lib.getOptArgs("test").size());
        assertEquals(1,lib.getReqArgs("test").size());
    }

}